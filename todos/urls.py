from django.urls import path
from todos.views import todo_list, todo_list_detail, edit_list, create_list

urlpatterns = [
    path("create/", create_list, name="create_list"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list, name="todo_list"),
]
